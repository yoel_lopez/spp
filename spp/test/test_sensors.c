#include "unity.h"
#include "sensor.h"

void test_SensorVentanaOffAfterCreate(void){
	uint16_t sensorVentana = 0xFFFF;
    Sensor_Create(&sensorVentana);
    TEST_ASSERT_EQUAL_HEX16(0,sensorVentana);
}

//void test_SensorPuertaOffAfterCreate(void){
//	uint16_t sensorPuerta = 0xFFFF;
//   Sensor_Create(&sensorPuerta);
//    TEST_ASSERT_EQUAL_HEX16(0,sensorPuerta);
//}

void test_SensorVentanaTurnOn(void){
	uint16_t sensorVentana = 0xFFFF;
    Sensor_TurnOn(&sensorVentana);
    TEST_ASSERT_EQUAL_HEX16(1,sensorVentana);

}

//void test_SensorPuertaTurnOn(void){
//	uint16_t sensorPuerta = 0x1;
//   Sensor_TurnOn(&sensorPuerta);
//   TEST_ASSERT_EQUAL_HEX16(1,sensorPuerta);
//}

void test_SensorVentanaCheckState(void){
	uint16_t sensorVentana = 0x1;
	uint16_t estado = 0xFFFF;
    Sensor_CheckState(&sensorVentana,&estado);
    TEST_ASSERT_EQUAL_HEX16(1,estado);

}

//void test_SensorPuertaCheckState(void){
//	uint16_t sensorPuerta = 0x1;
//	uint16_t estado = 0x1;
//    Sensor_CheckState(&sensorPuerta,&estado);
//    TEST_ASSERT_EQUAL_HEX16(1,estado);
//}