#include "unity.h"
#include "alarma.h"
#include "sensor.h"

void test_AlarmaCheckStateArmed(void){
	uint16_t password = 1234;
	uint16_t estado = 0xFFFF;
    Alarm_CheckStateArmed(&password,&estado);
    TEST_ASSERT_EQUAL_HEX16(1,estado);
}

void test_AlarmaCheckStateDisarmed(void){
	uint16_t password = 123;
	uint16_t estado = 0xFFFF;
    Alarm_CheckStateDisarmed(&password,&estado);
    TEST_ASSERT_EQUAL_HEX16(2,estado);
}

void test_ActivateAlarma(void){
	uint16_t sensorVentana = 0x1;
	uint16_t estadoSensor = 0xFFFF;
	uint16_t estadoAlarma = 0xFFFF;
	uint16_t estadoSistema = 0xFFFF;
	uint16_t password = 1234;
    Sensor_CheckState(&sensorVentana,&estadoSensor);
    Alarm_CheckStateArmed(&password,&estadoAlarma);
    Alarm_Activate(&estadoSensor,&estadoAlarma,&estadoSistema);
    TEST_ASSERT_EQUAL_HEX16(1,estadoSistema);
}
