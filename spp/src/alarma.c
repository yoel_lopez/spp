#include "alarma.h"


void Alarm_CheckStateArmed(uint16_t * password,uint16_t * estado){
	if( * password == 1234) {
		*estado = 1;
	}
}

void Alarm_CheckStateDisarmed(uint16_t * password,uint16_t * estado){
	if( * password == 123) {
		*estado = 2;
	}
}

void Alarm_Activate(uint16_t * estadoSensor,uint16_t * estadoAlarma,uint16_t * estadoSistema){

	if( *estadoSensor == 1 & *estadoAlarma == 1) {
		*estadoSistema = 1;
	}
}
