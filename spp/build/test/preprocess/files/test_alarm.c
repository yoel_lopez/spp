#include "build/temp/_test_alarm.c"
#include "sensor.h"
#include "alarma.h"
#include "unity.h"


void test_AlarmaCheckStateArmed(void){

 uint16_t password = 1234;

 uint16_t estado = 0xFFFF;

    Alarm_CheckStateArmed(&password,&estado);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((estado)), (((void *)0)), (UNITY_UINT)(9), UNITY_DISPLAY_STYLE_HEX16);

}



void test_AlarmaCheckStateDisarmed(void){

 uint16_t password = 123;

 uint16_t estado = 0xFFFF;

    Alarm_CheckStateDisarmed(&password,&estado);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((2)), (UNITY_INT)(UNITY_INT16)((estado)), (((void *)0)), (UNITY_UINT)(16), UNITY_DISPLAY_STYLE_HEX16);

}



void test_ActivateAlarma(void){

 uint16_t sensorVentana = 0x1;

 uint16_t estadoSensor = 0xFFFF;

 uint16_t estadoAlarma = 0xFFFF;

 uint16_t estadoSistema = 0xFFFF;

 uint16_t password = 1234;

    Sensor_CheckState(&sensorVentana,&estadoSensor);

    Alarm_CheckStateArmed(&password,&estadoAlarma);

    Alarm_Activate(&estadoSensor,&estadoAlarma,&estadoSistema);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((estadoSistema)), (((void *)0)), (UNITY_UINT)(28), UNITY_DISPLAY_STYLE_HEX16);

}
