#include "build/temp/_test_sensors.c"
#include "sensor.h"
#include "unity.h"


void test_SensorVentanaOffAfterCreate(void){

 uint16_t sensorVentana = 0xFFFF;

    Sensor_Create(&sensorVentana);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((0)), (UNITY_INT)(UNITY_INT16)((sensorVentana)), (((void *)0)), (UNITY_UINT)(7), UNITY_DISPLAY_STYLE_HEX16);

}















void test_SensorVentanaTurnOn(void){

 uint16_t sensorVentana = 0xFFFF;

    Sensor_TurnOn(&sensorVentana);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((sensorVentana)), (((void *)0)), (UNITY_UINT)(19), UNITY_DISPLAY_STYLE_HEX16);



}















void test_SensorVentanaCheckState(void){

 uint16_t sensorVentana = 0x1;

 uint16_t estado = 0xFFFF;

    Sensor_CheckState(&sensorVentana,&estado);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((estado)), (((void *)0)), (UNITY_UINT)(33), UNITY_DISPLAY_STYLE_HEX16);



}
